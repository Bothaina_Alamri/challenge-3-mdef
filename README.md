# Challenge 3 MDEF

The documentation of challenge3

<html
</head>
<body>
<h1>Face Mask Sterilizing Unit / silicone face mold</h1>

<h2>The third challenge was connected to our 2 last challenges of the face mask mold / this project was about developing a container to sterilize face masks in a suitable form for home users</h2>

​<picture>
 <source srcset="6.jpg" type="image/svg+xml">
 <img src="6.jpg" class="img-fluid img-thumbnail" width="900" height="500" alt="">
</picture>

<h3>The sterilizing unit needed two main things </h3>
<h5>-A solid box to contain the mold and protect the human from the sterilization process</h5>
<h5>-A UV light to sterilize the face masks and kill the bacteria</h5>



<h3>To start we needed a box that is a bit bigger than the face mask mold that we wanted to sterilize and the perfect box was a 33 cm cake box ,  After that, we needed to paint the box to make it more opaque to be certain that the light will not get out of the box</h3>

<picture>
 <source srcset="7.jpg" type="image/svg+xml">
 <img src="7.jpg" class="img-fluid img-thumbnail" width="500" height="500" alt="">
</picture>

<h3>Then we started the sterilizing light process, for the challenge we used an LED light instead of the UV light ( type UVC ) to make the prototype safer</h3>

<h3>To control the light we used a ESP32 controller, The code was written to make the box safer / more secure for use by doing these 3 functions,<h3>

<h5>-turn the light off if the box is open.<h5>
<h5>-keep the light off unless we pressed the button on the top.<h5>
<h5>-turn the light off after 10 min of bushing the button. </h5>

<img src="2.jpg" class="rounded float-left" width="500" height="500" alt="">
<img src="3.jpg" class="rounded float-right" width="500" height="500" alt="">


<h3> The ESP32 controller code <h3>
<h6>int ledpin = 14;
int detectionpin = 27;
int val = 0;
int button = 15;
bool buttonpressed = false;
bool buttonv = false;
long previousMillis = 0;        // will store last time LED was updated
// the follow variables is a long because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
long interval = 5000; // 10 mins
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
 // pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);
  pinMode(ledpin,OUTPUT);
  pinMode(detectionpin,INPUT);
  pinMode(button,INPUT);
  //digitalWrite(ledpin,HIGH);
  buttonpressed = false;
}
//the loop function runs over and over agin forever
void loop () {
 //buttonpressed =
 buttonv = digitalRead(button);
 if (buttonv == true){
  buttonpressed = true;
 }
  if(buttonpressed == true){
    Serial.print("button pressed");  
    unsigned long currentMillis = millis();
    if(currentMillis - previousMillis > interval) {
      // save the last time you blinked the LED
      previousMillis = currentMillis;
      buttonpressed = false;
      digitalWrite(ledpin,LOW);
      }
      else{
      val = digitalRead(detectionpin);
      Serial.print(val);
        if(val == 1){
        digitalWrite(ledpin, HIGH);
        }
        else{
          digitalWrite(ledpin,LOW);
        }
      delay(10);
    }            
  }
}</h6>

<h3> The next steps will be </h3>
<h5>-Design a box that will fit the image of UPBABE! </h5>
<h5> -Understanding the location of the unit at the user home to make it more fitting to the home environment </h5>
<h5>-Studying the level of sterilization of the UV light </h5>
<h5>-Making the unit more portable </h5>


<h2>The second part of the challenge was to make a silicone mold for the face masks to try new materials and texters to understand the new possibilities of the face masks</h2>
<img src="11.jpg" class="rounded float-right" width="500" height="500" alt="">
<img src="13.jpg" class="rounded float-right" width="500" height="500" alt="">

<h3>We started by creating the mold on Rhino then printing it in the 3d printer.</h3>
<h5>[3D printing file.](Files/facerhino.stl)</h5>


<img src="17.jpg" class="rounded float-right" width="500" height="500" alt="">
<img src="16.jpg" class="rounded float-right" width="500" height="500" alt="">

<h3>We used the printed mold to create the negative silicone mold</h3>

<img src="14.jpg" class="rounded float-right" width="500" height="500" alt="">
<img src="12.jpg" class="rounded float-right" width="500" height="500" alt="">

<h3>The next steps will be </h3>
<h5>-Using different materials to create the face mold </h5>
<h5>-Using different texters for the mold </h5>
<h5>-Using sustainable / reusable materials to create the face mold</h5>

<h3> Project references </h3>
<p class="w3-large ">
<a href="https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/">(1) Installing the ESP32 Board in Arduino IDE (Windows, Mac OS X, Linux)</a></p>
<a href="https://create.arduino.cc/projecthub/Arnov_Sharma_makes/uvc-box-a-diy-uv-sterilizer-f09b8f">(2)UVC Box" a DIY UV Sterilizer</a></p>
<a href="https://www.instructables.com/Capacitive-Switch-for-Arduino/">(3) Capacitive Switch for Arduino</a></p>
<a href="https://forums.adafruit.com/viewtopic.php?f=22&t=91868">(4) Feather Huzzah Not Detected</a></p>
<a href="https://www.arduino.cc/en/Tutorial/BuiltInExamples/StateChangeDetection">(5) State Change Detection (Edge Detection) for pushbuttons</a></p>
<a href="https://learn.adafruit.com/multi-tasking-the-arduino-part-1/using-millis-for-timing">(6) Using millis() for timing</a></p>
<a href="https://www.amazon.fr/Sundis-7239002-Fresh-Boîte-Plastique/dp/B01C4D3N40">(7) Sterilizing cake Box</a></p>
<a href="https://www.amazon.fr/stérilisation-désinfectant-téléphone-diffuseur-daromathérapie/dp/B08BYMQR2S">(8) UV sterilization box</a></p>
</body>
</html>
